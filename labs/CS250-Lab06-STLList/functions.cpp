#include <iostream>         // Use input and output streams
#include <string>           // Using string (for text)
#include <iomanip>          // Special library for formatting text (used in my tests)
#include <cstdlib>          // Random numbers
#include <fstream>          // File input/output
#include <list>             // STL List
using namespace std;        // Use the STandarD library

#include "functions.hpp"    // Function headers are stored here

// USER-DEFINED FUNCTIONS - You will edit these!

//! Add the customer to the back of the line of customers
void CustomerEntersBackOfLine( list<Customer>& line, Customer customer )
{
}

//! Add the customer to the front of the line of customers
void CustomerEntersFrontOfLine( list<Customer>& line, Customer customer )
{
}

//! Remove the customer at the front of the line
void CustomerOrders( list<Customer>& line )
{
}

//! Remove the customer at the back of the line
void CustomerRagequits( list<Customer>& line )
{
}

//! Get the customer at the front of the line
Customer GetLineFront( list<Customer>& line )
{
	return Customer();	// temporary
}

//! Get the customer at the back of the line
Customer GetLineBack( list<Customer>& line )
{
	return Customer();	// temporary
}

//! Create a string that lists all customers in line
string GetLine( list<Customer>& line )
{
    return ""; // temporary
}



/** Program code **/
void Program()
{
    float balance = 0;
    int timeCounter = 0;
    int hour = 8;
    int minute = 0;
    int id = 0;
    bool done = false;

    ofstream output( "donut-sim.html" );

    list<Customer> customerLine;

    ClearScreen();
    output << "<head><title>Jelly Donuts!</title></head><body>" << endl;
    output << "<style>" << endl;
    output << " .lineup { background: #9dcef0; font-weight:bold; padding: 10px; }" << endl;
    output << " .coupon { background: #f5e788; font-weight:bold; padding: 10px; }" << endl;
    output << " .serve { background: #98f588; font-weight:bold; padding: 10px; }" << endl;
    output << " .quit { background: #fabbc5; font-weight:bold; padding: 10px; }" << endl;
    output << " .event { padding:10px; margin-bottom: 50px; border: solid 2px #aaaaaa; }" << endl;
    output << " table { width: 100%; }" << endl;
    output << " table td { width: 25%; }" << endl;
    output << "</style>" << endl;
    output << "<h1>JCCC Jelly Donuts Shoppe Simulator</h1>" << endl;

    while ( !done )
    {
        timeCounter++;
        output << "<div class='event'>" << endl;
        output << "<table><tr><td>Time: " << hour << ":";
        if ( minute < 10 )  output << "0" << minute;
        else                output << minute;
        output << "</td><td>" << customerLine.size();
        if ( customerLine.size() == 1 )     output << " customer";
        else                                output << " customers";
        output << " in line...</td><td><strong>Current balance:</strong> $" << fixed << setprecision(2) << balance << "</td>";
        output << "<td><strong>Current line:</strong> " << GetLine( customerLine ) << "</td>";
        output << "</tr></table>";

        minute += 10;
        if ( minute == 60 )
        {
            hour++;
            if ( hour > 12 )
            {
                hour = 1;
            }
            minute = 0;
        }

        Action action = Action( rand() % 4 );
        if ( action == LINEUP )
        {
            Customer newCustomer = MakeRandomCustomer( ++id );
            CustomerEntersBackOfLine( customerLine, newCustomer );
            output << "<p class='lineup'>Customer " << newCustomer.name << " (" << newCustomer.id << ") gets in line!</p>";
        }
        else if ( action == COUPON )
        {
            Customer newCustomer = MakeRandomCustomer( ++id );
            CustomerEntersFrontOfLine( customerLine, newCustomer );
            output << "<p class='coupon'>Customer " << newCustomer.name << " (" << newCustomer.id << ") has a special coupon and enters the line at the front!</p>";
        }
        else if ( action == SERVE )
        {
            if ( customerLine.size() > 0 )
            {
                Customer customer = GetLineFront( customerLine );
                output << "<p class='serve'>Customer " << customer.name << " (" << customer.id << ") orders a "
                    << customer.order.name << " donut for $" << customer.order.price << " and leaves!</p>";
                balance += customer.order.price;
                CustomerOrders( customerLine );
            }
            else
            {
                output << "Everyone stands around awkwardly...";
            }
        }
        else if ( action == RAGEQUIT )
        {
            if ( customerLine.size() > 0 )
            {
                Customer customer = GetLineBack( customerLine );
                output << "<p class='quit'>Customer " << customer.name << " (" << customer.id << ") is tired of waiting and leaves the line!</p>";
                CustomerRagequits( customerLine );
            }
            else
            {
                output << "Everyone stands around awkwardly...";
            }
        }

        output << "</div>" << endl << endl;

        if ( timeCounter == 72 )
        {
            done = true;
        }
    }

    output << "<p><strong>Store closed for the day!</strong></p>" << endl;

    output << "<p>Total money earned: $" << fixed << setprecision(2) << balance << "</p>" << endl;

    output << "</body>" << endl;

    cout << "Simulation output saved to \"donut-sim.html\"" << endl;
    Pause();
}

// Tester functions (DO NOT MODIFY) ----------------------------------------------------
void RunTests()
{
    Test_Set1();
    Test_Set2();
    Test_Set3();
    Test_Set4();
    Test_Set5();
    Test_Set6();
    Test_Set7();
}

void Test_Set1()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - CustomerEntersBackOfLine" << endl;
    list<Customer> line;
    Customer customer1, customer2;
    customer1.name = "name1";
    customer2.name = "name2";

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that customer \"name1\" is added correctly." << setw( pfWidth );
    CustomerEntersBackOfLine( line, customer1 );

    if ( line.size() == 0 )                         { cout << " x FAIL\n\t Size is 0 after adding customer." << endl; }
    else if ( line.back().name != customer1.name )  { cout << " x FAIL\n\t Customer was not found at back position.";
                                                      cout << "       \n\t EXPECTED: \"" << customer1.name << "\" \n\t ACTUAL:   \"" << line.back().name << "\"" << endl; }
    else                                            { cout << " * PASS" << endl; }



    cout << endl << left << setw( headerWidth ) << "TEST 2: Check that customer \"name2\" is added correctly." << setw( pfWidth );
    CustomerEntersBackOfLine( line, customer2 );

    if ( line.size() == 0 )                         { cout << " x FAIL\n\t Size is 0 after adding customer." << endl; }
    else if ( line.back().name != customer2.name )  { cout << " x FAIL\n\t Customer was not found at back position.";
                                                      cout << "       \n\t EXPECTED: \"" << customer2.name << "\" \n\t ACTUAL:   \"" << line.back().name << "\"" << endl; }
    else                                            { cout << " * PASS" << endl; }

    if ( line.size() == 0 )
    {
        cout << endl << "CANNOT CONTINUE TEST - SEGFAULT RISK. ENDING EARLY." << endl;
        return;
    }

    cout << endl << left << setw( headerWidth ) << "TEST 3: Check that items are in the correct positions." << setw( pfWidth );
    if ( line.front().name == customer1.name && line.back().name == customer2.name ) { cout << " * PASS" << endl; }
    else { cout << " x FAIL\n\tCustomers not in the correct positions.";
           cout << "       \n\t EXPECTED: \"" << customer1.name << " " << customer2.name << "\""
                << "       \n\t ACTUAL:   \"" << line.front().name << " " << line.back().name << "\"" << endl; }
}

void Test_Set2()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - CustomerEntersFrontOfLine" << endl;
    list<Customer> line;
    Customer customer1, customer2;
    customer1.name = "name1";
    customer2.name = "name2";

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that customer \"name1\" is added correctly." << setw( pfWidth );
    CustomerEntersFrontOfLine( line, customer1 );

    if ( line.size() == 0 )                          { cout << " x FAIL\n\t Size is 0 after adding customer." << endl; }
    else if ( line.front().name != customer1.name )  { cout << " x FAIL\n\t Customer was not found at back position.";
                                                       cout << "       \n\t EXPECTED: \"" << customer1.name << "\" \n\t ACTUAL:   \"" << line.front().name << "\"" << endl; }
    else                                             { cout << " * PASS" << endl; }



    cout << endl << left << setw( headerWidth ) << "TEST 2: Check that customer \"name2\" is added correctly." << setw( pfWidth );
    CustomerEntersFrontOfLine( line, customer2 );

    if ( line.size() == 0 )                          { cout << " x FAIL\n\t Size is 0 after adding customer." << endl; }
    else if ( line.front().name != customer2.name )  { cout << " x FAIL\n\t Customer was not found at back position.";
                                                       cout << "       \n\t EXPECTED: \"" << customer2.name << "\" \n\t ACTUAL:   \"" << line.front().name << "\"" << endl; }
    else                                             { cout << " * PASS" << endl; }

    if ( line.size() == 0 )
    {
        cout << endl << "CANNOT CONTINUE TEST - SEGFAULT RISK. ENDING EARLY." << endl;
        return;
    }

    cout << endl << left << setw( headerWidth ) << "TEST 3: Check that items are in the correct positions." << setw( pfWidth );
    if ( line.front().name == customer2.name && line.back().name == customer1.name ) { cout << " * PASS" << endl; }
    else { cout << " x FAIL\n\tCustomers not in the correct positions.";
           cout << "       \n\t EXPECTED: \"" << customer2.name << " " << customer1.name << "\""
                << "       \n\t ACTUAL:   \"" << line.front().name << " " << line.back().name << "\"" << endl; }
}

void Test_Set3()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - CustomerOrders" << endl;
    list<Customer> line;
    Customer customer1, customer2;
    customer1.name = "name1";
    customer2.name = "name2";

    CustomerEntersBackOfLine( line, customer1 );
    CustomerEntersBackOfLine( line, customer2 );

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that customer is removed." << setw( pfWidth );
    CustomerOrders( line );

    if ( line.size() != 1 )                             { cout << " x FAIL\n\t Size of line is wrong." << endl; }
    else if ( line.front().name != customer2.name )     { cout << " x FAIL\n\t Front customer is wrong." << endl; }
    else                                                { cout << " * PASS" << endl; }
}

void Test_Set4()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - CustomerRagequits" << endl;
    list<Customer> line;
    Customer customer1, customer2;
    customer1.name = "name1";
    customer2.name = "name2";

    CustomerEntersBackOfLine( line, customer1 );
    CustomerEntersBackOfLine( line, customer2 );

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that customer is removed." << setw( pfWidth );
    CustomerRagequits( line );

    if ( line.size() != 1 )                             { cout << " x FAIL\n\t Size of line is wrong." << endl; }
    else if ( line.back().name != customer1.name )      { cout << " x FAIL\n\t Back customer is wrong." << endl; }
    else                                                { cout << " * PASS" << endl; }


}

void Test_Set5()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetLineFront" << endl;
    list<Customer> line;
    Customer customer1, customer2;
    customer1.name = "name1";
    customer2.name = "name2";

    CustomerEntersBackOfLine( line, customer1 );
    CustomerEntersBackOfLine( line, customer2 );

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that correct customer is returned" << setw( pfWidth );
    Customer customer = GetLineFront( line );

    if ( customer.name != customer1.name )  {  cout << " x FAIL\n\t Wrong customer was returned as front." << endl;
                                               cout << "       \n\t EXPECTED: \"" << customer1.name << "\""
                                                    << "       \n\t ACTUAL:   \"" << customer.name << "\"" << endl; }
    else                                    {  cout << " * PASS" << endl; }

}

void Test_Set6()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetLineBack" << endl;
    list<Customer> line;
    Customer customer1, customer2;
    customer1.name = "name1";
    customer2.name = "name2";

    CustomerEntersBackOfLine( line, customer1 );
    CustomerEntersBackOfLine( line, customer2 );

    cout << endl << left << setw( headerWidth ) << "TEST 1: Check that correct customer is returned" << setw( pfWidth );
    Customer customer = GetLineBack( line );

    if ( customer.name != customer2.name )  {  cout << " x FAIL\n\t Wrong customer was returned as front." << endl;
                                               cout << "       \n\t EXPECTED: \"" << customer2.name << "\""
                                                    << "       \n\t ACTUAL:   \"" << customer.name << "\"" << endl; }
    else                                    {  cout << " * PASS" << endl; }

}

void Test_Set7()
{
    cout << endl << "---------------------------------------------------" << endl;
    cout << "Test - GetLine" << endl;
    list<Customer> line;
    Customer customer1, customer2, customer3;
    customer1.name = "name1";
    customer2.name = "name2";
    customer3.name = "name2";

    CustomerEntersBackOfLine( line, customer1 );
    CustomerEntersBackOfLine( line, customer2 );
    CustomerEntersBackOfLine( line, customer3 );

    cout << endl << left << setw( headerWidth ) << "TEST 1: Get list of customers" << setw( pfWidth );
    string lineString = GetLine( line );

    if (    lineString.find( customer1.name ) == string::npos ||
            lineString.find( customer2.name ) == string::npos ||
            lineString.find( customer3.name ) == string::npos )
    {
        cout << " x FAIL\n\t Missing customer(s) in list." << endl;
        cout << "       \n\t EXPECTED: name1 name2 name3"
             << "       \n\t ACTUAL:  " << lineString << endl;
    }
    else { cout << " * PASS" << endl; }

}


string B2S( bool val )
{
    return ( val ) ? "true" : "false";
}

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    system( "cls" );
    #else
    system( "clear" );
    #endif
}

void Pause()
{
    cout << "Press enter to continue..." << endl;
    cin.ignore();
    cin.get();
}

Customer MakeRandomCustomer( int id )
{
    Customer c;
    c.order = ORDER[ rand() % ORDER.size() ];
    c.name = NAMES[ rand() % NAMES.size() ];
    c.id = id;
    return c;
}
