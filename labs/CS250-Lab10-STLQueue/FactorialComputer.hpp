#ifndef COMPUTER_HPP
#define COMPUTER_HPP

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <queue>
using namespace std;

class FactorialComputer
{
    public:
    FactorialComputer();
    void Run();
    void CreateJobs( int jobCount );
    void ProcessJobs();

    private:
    queue<int> m_jobQueue;

    int Factorial_Rec( int n );
};

FactorialComputer::FactorialComputer()
{
    srand( time( NULL ) );
}

void FactorialComputer::Run()
{
    cout << "How many jobs to create? ";
    int amount;
    cin >> amount;

    CreateJobs( amount );

    cout << m_jobQueue.size() << " items to process in the job queue." << endl << endl;

    ProcessJobs();
}

int FactorialComputer::Factorial_Rec( int n )
{
    if ( n == 0 )
    {
        return 1;
    }

    return n * Factorial_Rec( n-1 );
}

/**
    @param int jobCount     The amount of jobs to create

    Create a random number between 0 and 14 with rand() % 15.
    Push this number into the job queue.
*/
void FactorialComputer::CreateJobs( int jobCount )
{
}

/**
    While the job queue is not empty...
    - Display the front item
    - Call Factorial_Rec, passing in the front item. Output the result.
    - Pop the front item off the queue.
    - Display the amount of jobs remaining.
*/
void FactorialComputer::ProcessJobs()
{
}

#endif
