#include "Tester.hpp"
#include "Program.hpp"
#include "cuTEST/Menu.hpp"

#include "DataStructure/LinkedList.hpp"

#include <iostream>
using namespace std;

int main()
{
    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "PROJECT 2 MAIN MENU" );
        int choice = Menu::ShowIntMenuWithPrompt( { "Run tester", "Run program", "Quit" } );

        switch( choice )
        {
            case 1:
            {
                Tester tester;
                tester.Start();
                Menu::Pause();
            }
            break;

            case 2:
            cout << "?";
            break;

            case 3:
            done = true;
            break;
        }
    }

    return 0;
}
